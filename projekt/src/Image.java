import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Image {
    private BufferedImage image;
    private String outputFormat;

    public Image(String path, String outputFormat)
    {
        this.outputFormat = outputFormat;
        try {
            this.image = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveImageToFile(String path)
    {
        try {
            ImageIO.write(this.image, this.outputFormat, new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void scale(double coefficient)
    {
        this.image = Scale.scale(this.image, coefficient);
    }


    public void entropy(int sizeX, int sizeY)
    {
        this.image = Entropy.entropy(this.image, sizeX, sizeY);
    }

    public void erosionBinary()
    {
        this.image = Erosion.getErosionBinary(this.image, Arrays.oneBuffor(3,3));
    }

    public void dilatationBinary()
    {
        this.image = Dilation.getDilationBinary(this.image,3,3  );
    }

    public void open(double length, double slope)
    {
        double[][] structure = Arrays.getCircle(length,slope);
        this.image = Open.getOpne(this.image,structure);
    }

    public void bwdist(String method)
    {

        this.image = Bwdist.getBwdist(this.image,method);
    }
}
