import java.awt.*;
import java.awt.image.BufferedImage;

public class Dilation {
    public static BufferedImage getDilationBinary(BufferedImage image, int sizeX, int sizeY)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                Color color = new Color(image.getRGB(row,column));
                if ( color.getRed() == 1)
                {
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                    continue;
                }

                Color[][] colors = Kolory.getBufer(image,row,column,sizeX,sizeY);
                double[][] buffer = Kolory.getRed(colors,sizeX,sizeY);

                double[][] structure = Arrays.oneBuffor(sizeX,sizeY);

                if ( Arrays.isInToOther(buffer,structure))
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                else
                    newImage.setRGB(row,column,new Color(0.0f,0.0f,0.0f).getRGB());

            }
        }

        return newImage;
    }

    public static BufferedImage getDilationBinary(BufferedImage image, double[][] structure)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                Color color = new Color(image.getRGB(row,column));
                if ( color.getRed() == 1)
                {
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                    continue;
                }

                Color[][] colors = Kolory.getBufer(image,row,column,structure.length,structure[0].length);
                double[][] buffer = Kolory.getRed(colors,structure.length,structure[0].length);

                if ( Arrays.isInToOther(buffer,structure))
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                else
                    newImage.setRGB(row,column,new Color(0.0f,0.0f,0.0f).getRGB());

            }
        }

        return newImage;
    }

    public static BufferedImage getDilationMono(BufferedImage image, int sizeX, int sizeY)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                Color color = new Color(image.getRGB(row,column));
                if ( color.getRed() == 1)
                {
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                    continue;
                }

                Color[][] colors = Kolory.getBufer(image,row,column,sizeX,sizeY);
                double[][] buffer = Kolory.getRed(colors,sizeX,sizeY);

                double value = Arrays.getMaxValue(buffer);

                newImage.setRGB(row, column,new Color((float)value, (float) value, (float)value).getRGB());

            }
        }

        return newImage;
    }
}
