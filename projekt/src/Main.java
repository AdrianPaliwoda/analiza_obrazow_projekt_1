import java.awt.image.BufferedImage;

public class Main {

    public static void main(String[] args) {
        Image scale = new Image("p0.bmp", "bmp");
        scale.scale(0.5);
        scale.saveImageToFile("p1.bmp");

        Image entropy = new Image("monochrome.jpg", "jpg");
        entropy.entropy(9,9);
        entropy.saveImageToFile("monoEntropy.jpg");


        Image opening = new Image("binar2.png","png");
        opening.open(8,45);
        opening.saveImageToFile("open.png");

        Image bwdist1 = new Image("binar2.png","png");
        bwdist1.bwdist("chessboard");
        bwdist1.saveImageToFile("bwdist.png");

        Image bwdist2 = new Image("skaly.png","png");
        bwdist2.bwdist("euclidean");
        bwdist2.saveImageToFile("bwdist2.png");
    }
}
