import java.awt.*;
import java.awt.image.BufferedImage;

public class Kolory extends BiCubic {
    public static double[][] getRed(Color[][] color, int row, int column)
    {
        double[][] red = new double[row][column];

        for (int i = 0; i < row; i++)
            for (int j = 0; j < column; j++)
                red[i][j] = color[i][j].getRed() / 255.0;

        return red;
    }

    public static double[][] getGreen(Color[][] color, int row, int column)
    {
        double[][] red = new double[row][column];

        for (int i = 0; i < row; i++)
            for (int j = 0; j < column; j++)
                red[i][j] = color[i][j].getGreen() / 255.0;

        return red;
    }

    public static double[][] getBlue(Color[][] color, int row, int column)
    {
        double[][] red = new double[row][column];

        for (int i = 0; i < row; i++)
            for (int j = 0; j < column; j++)
                red[i][j] = color[i][j].getBlue() / 255.0;

        return red;
    }

    public static Color[][] getBufer(BufferedImage image, int a, int b, int sizeX, int sizeY)
    {
        Color[][] buffer = zeroColorsBuffor(sizeX,sizeY);

        int x = getStartImage(a,sizeX);
        int y = getStartImage(b,sizeY);
        int EndIndexX = getEndIndexBuffor(image.getWidth(),x, sizeX);
        int EndIndexY = getEndIndexBuffor(image.getHeight(),y, sizeY);

        int sizeBufforY = Arrays.getBufforSize(b, sizeY, image.getHeight());

        for (int i = getStartIndexBuffor(a,sizeX);i < EndIndexX; i++)
        {
            for (int j = getStartIndexBuffor(b, sizeY); j < EndIndexY; j++)
            {
               Color color = new Color(image.getRGB(x, y++));
               buffer[i][j] = color;
            }
            x++;
            y -= sizeBufforY;
        }

        return buffer;
    }

    private static int getEndIndexBuffor(int width,int index, int size)
    {
        if ( index + size > width )
            return size - (index + size - width);
        return size;
    }

    public static Color[][] zeroColorsBuffor(int sizeX, int sizeY)
    {
        Color[][] buffer = new Color[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                buffer[i][j] = new Color(0,0,0);
            }
        }

        return buffer;
    }

     public static Color[][] oneColorsBuffor(int sizeX, int sizeY)
    {
        Color[][] buffer = new Color[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                buffer[i][j] = new Color(1.0f,1.0f,1.0f);
            }
        }

        return buffer;
    }

    public static int getStartIndexBuffor(int index, int size)
    {
        int x = index - size / 2;
        if ( x < 0)
            return -x;
        return 0;
    }

    public static int getStartImage(int index,int size)
    {
        int x = index - size/ 2;
        if ( x < 0)
            return index;
        return x;
    }



    public static void setColor(BufferedImage newImage, Color[][] colors, int row, int column, int sizeX, int sizeY)
    {
        int i = 0;
        int j = 0;

        for (int x = row; x < (row+sizeX ); x++)
        {
            for (int y = column; y < (column + sizeY ); y++)
            {
                 newImage.setRGB(x,y,colors[i][j++].getRGB());
            }
            i++;
            j = 0;
        }
    }

    public static int whichColor(Color[][] colors)
    {
        for(int i = 0 ; i < colors.length; i++)
        {
            for (int j = 0; j < colors[0].length; j++)
            {
                if (colors[i][j].getRed() != 0)
                    return  0;
                else if (colors[i][j].getGreen() != 0)
                    return 1;
                else if (colors[i][j].getBlue() != 0)
                    return 2;
            }
        }
        return -1;
    }

    public static void setColorToImage(BufferedImage newImage, int i, double entropy, int row, int column)
    {
        if ( i == 0)
            newImage.setRGB(row, column, new Color((float) (entropy), 0 , 0).getRGB());
        if ( i == 1)
            newImage.setRGB(row, column, new Color(0, (float) (entropy), 0).getRGB());
        if ( i == 2)
            newImage.setRGB(row, column, new Color(0, 0 , (float) (entropy)).getRGB());
    }

}
