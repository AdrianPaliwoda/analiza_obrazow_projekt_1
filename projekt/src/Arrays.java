public class Arrays {

    public static double[][] zeroBuffor(int sizeX, int sizeY)
    {
        double[][] buffer = new double[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                buffer[i][j] = 0;
            }
        }

        return buffer;
    }
    public static double[][] oneBuffor(int sizeX, int sizeY)
    {
        double[][] buffer = new double[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                buffer[i][j] = 1;
            }
        }

        return buffer;
    }

    public static int getBufforSize(int index, int sizeBuffor, int sizeImage)
    {
        int begin = index - (sizeBuffor / 2);
        int end = index + (sizeBuffor / 2);
        int diff = 0;

        if ( begin < 0)
            diff -= begin;
        if ( end > sizeImage)
            diff = end - sizeImage;

        return sizeBuffor - diff;
    }

    public static boolean isArrayTheSame(double[][] array1, double[][] array2)
    {
        if ( (array1.length != array2.length) || (array1[0].length != array2[0].length) )
            return false;

        for ( int i = 0; i < array1.length; i++)
        {
            for (int j = 0; j < array1[0].length; j++)
            {
                if ( array1[i][j] != array2[i][j])
                    return false;
            }
        }
        return true;
    }

    public static boolean isZeroInside(double[][] array)
    {
        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                if( array[i][j] == 0)
                    return true;
            }
        }

        return false;
    }

    public static boolean isOneInside(double[][] array)
    {
        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                if( array[i][j] == 1.0)
                    return true;
            }
        }

        return false;
    }

    public static boolean isFitToOther(double[][] array, double[][] structure)
    {
        if ( array.length != structure.length || array[0].length != structure[0].length)
            return false;
        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                if( structure[i][j] == 0.0)
                    continue;
                if ( structure[i][j] != array[i][j])
                    return false;
            }
        }
        return true;

    }

    public static boolean isInToOther(double[][] array, double[][] structure)
    {
        if ( array.length != structure.length || array[0].length != structure[0].length)
            return false;
        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                if( structure[i][j] == 0.0)
                    continue;
                if ( structure[i][j] == array[i][j])
                    return true;
            }
        }

        return false;

    }

    public static void negativeBooleanArray(double[][] array)
    {
        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                if ( array[i][j] == 0)
                    array[i][j] = 1;
                else
                    array[i][j] = 0;
            }
        }
    }

    public static double getMinValue(double[][] array)
    {
        double min = array[0][0];

        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
               if(min > array[i][j])
                   min = array[i][j];
            }
        }
        return min;
    }

    public static double getMaxValue(double[][] array)
    {
        double max = array[0][0];

        for ( int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                if(max < array[i][j])
                    max = array[i][j];
            }
        }
        return max;
    }

    public double[] appearsCount(double[] original, int[] toCount)
    {
        for (int i = 0; i < toCount.length; i++)
        {
            if (toCount[i] > 0)
                original[toCount[i]] += 1;
        }

        return original;
    }

    public double[] clearArray(double[] array, int size)
    {
        for (int i = 0; i < size; i ++)
        {
            array[i] = 0;
        }
        return array;
    }
    public static double[][] getCircle(double length, double angle)
    {
        double[][] circle = Arrays.zeroBuffor((int)length,(int)length);

        angle = Math.toRadians(angle);

        int x = 0;
        int y = 0;

        for ( int i = 0; i < (int) length; i ++)
        {
            x = valueForX(0,i,angle);
            y = valueForY(0,i,angle);
            circle[x][y] = 1;
        }

        return circle;
    }

/*    public static double adjustAngle(double angle)
    {
        angle = toOnePeriod(angle);


    }*/

    public static int valueForX(double startX, double length, double angle)
    {
        return (int) (startX + length * Math.sin(angle));
    }

    public static int valueForY(double startY,double length, double angle)
    {
        return (int) (startY + length * Math.cos(angle));
    }

    public static void show2DArray(double[][] array)
    {
        for(int i = 0; i < array.length; i++)
        {
            for (int j = 0; j < array[0].length; j++)
            {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean ifStructureIsInBuffer(double[][] structure, double[][] buffer)
    {
        if ( buffer.length != structure.length || buffer[0].length != structure[0].length)
            return false;
        for ( int i = 0; i < buffer.length; i++)
        {
            for (int j = 0; j < buffer[0].length; j++)
            {
                if (structure[i][j] == 0)
                    continue;
                if(structure[i][j] != buffer[i][j])
                    return false;
            }
        }
        return true;
    }

}
