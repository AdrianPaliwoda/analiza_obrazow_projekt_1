import java.awt.*;
import java.awt.image.BufferedImage;

public class Erosion {

    public static BufferedImage getErosionBinary(BufferedImage image, int sizeX, int sizeY)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                Color color = new Color(image.getRGB(row,column));
                if ( color.getRed() == 0)
                    continue;

                Color[][] colors = Kolory.getBufer(image,row,column,sizeX,sizeY);
                double[][] buffer = Kolory.getRed(colors,sizeX,sizeY);

                double[][] structure = Arrays.oneBuffor(sizeX,sizeY);

                if ( Arrays.isArrayTheSame(structure,buffer))
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                else
                    newImage.setRGB(row,column,new Color(0.0f,0.0f,0.0f).getRGB());

            }
        }

        return newImage;
    }

    public static BufferedImage getErosionBinary(BufferedImage image, double[][] structure)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                Color color = new Color(image.getRGB(row,column));
                if ( color.getRed() == 0)
                    continue;

                Color[][] colors = Kolory.getBufer(image,row,column,structure.length,structure[0].length);
                double[][] buffer = Kolory.getRed(colors,structure.length,structure[0].length);

               // if ( Arrays.isArrayTheSame(structure,buffer))
                if ( Arrays.ifStructureIsInBuffer(structure,buffer))
                    newImage.setRGB(row,column,new Color(1.0f,1.0f,1.0f).getRGB());
                else
                    newImage.setRGB(row,column,new Color(0.0f,0.0f,0.0f).getRGB());

            }
        }

        return newImage;
    }

    public static BufferedImage getErosionMono(BufferedImage image, int sizeX, int sizeY)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                Color color = new Color(image.getRGB(row,column));
                if ( color.getRed() == 0)
                    continue;

                Color[][] colors = Kolory.getBufer(image,row,column,sizeX,sizeY);
                double[][] buffer = Kolory.getRed(colors,sizeX,sizeY);

                double value = Arrays.getMinValue(buffer);

                newImage.setRGB(row, column,new Color((float)value, (float) value, (float)value).getRGB());

            }
        }

        return newImage;
    }
}
