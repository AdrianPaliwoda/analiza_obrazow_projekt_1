import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class Entropy {


    public static BufferedImage entropy(BufferedImage image, int sizeX, int sizeY)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);


        for (int row = 0; (row + sizeX) < widthImage; row ++)
        {
            for (int column = 0; (column + sizeY) < heightImage; column ++)
            {
                Color[][] colors = Kolory.getBufer(image,row,column,sizeX,sizeY);
             /*   double[][] color = getOneColor(colors);
                double entropy = getDoubleEntropy(color,sizeX,sizeY);
*/
                Color colorEntropy = getColorEntropy(colors,sizeX,sizeY);

                newImage.setRGB(row,column,colorEntropy.getRGB());
            }
        }
        return newImage;
    }



    public static double entropyFormula(double[] probability)
    {
        double suma = 0;

        for (int i =0 ; i < probability.length; i++)
        {
            suma += entropyMultiply(probability[i]);
        }

        return -suma;
    }


    public static double entropyMultiply(double probability)
    {
        return 1.0 * ( Math.log(probability) / Math.log(2) ) * probability;
    }

    public double probability(double value, double omega)
    {
        return value / omega;
    }

    public static double[] posibilities(double[][] array, int sizeX, int sizeY)
    {
        Map<Double, Double> posibilities = new HashMap<>();
        double total = sizeX * sizeY;
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                posibilities.put(array[i][j],0.0);
            }
        }

        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                posibilities.put(array[i][j],posibilities.get(array[i][j]) + 1);
            }
        }

        double[] probability = new double[posibilities.size()];

        int i = 0;
        for(Map.Entry<Double, Double> entry : posibilities.entrySet()) {
            Double value = entry.getValue();

            probability[i++] = value / total;
        }
        return probability;
    }

    public double[] getProbability(double[] array, double total)
    {
        for (int i = 0; i < array.length; i++)
        {
            array[i] = array[i] / total;
        }

        return array;
    }


    private static Color getColorEntropy(Color[][] colors, int sizeX, int sizeY)
    {
        double[][] colorsRed = Kolory.getRed(colors,sizeX,sizeY);
        double[][] colorBlue = Kolory.getBlue(colors,sizeX,sizeY);
        double[][] colorGreen = Kolory.getGreen(colors,sizeX,sizeY);

        double entropyRed = getDoubleEntropy(colorsRed,sizeX,sizeY);
        double entropyBlue = getDoubleEntropy(colorBlue,sizeX,sizeY);
        double entropyGreen = getDoubleEntropy(colorGreen,sizeX,sizeY);

        return new Color((float)entropyRed,(float)entropyGreen, (float) entropyBlue);
    }

    private static double getDoubleEntropy(double[][] color, int sizeX, int sizeY)
    {
        return entropyFormula( posibilities(color,sizeX,sizeY)) / (sizeX * sizeY);
    }
    public double[][] getOneColor(Color[][] colors)
    {
        int numberColor = Kolory.whichColor(colors);

        if ( numberColor == 0 )
        {
            return Kolory.getRed(colors,colors.length,colors[0].length);
        }
        else if ( numberColor == 1 )
        {
            return Kolory.getGreen(colors,colors.length,colors[0].length);
        }
        else
        {
            return Kolory.getBlue(colors,colors.length,colors[0].length);
        }
    }

}
