import java.awt.*;
import java.awt.image.BufferedImage;

public class Bwdist {

    public static BufferedImage getBwdist(BufferedImage image,String method)
    {
        int widthImage = image.getWidth();
        int heightImage = image.getHeight();

        BufferedImage newImage = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);

        for(int row = 0; row < widthImage; row++)
        {
            for (int column = 0; column < heightImage; column++)
            {
                double maxMethod =  maxValueMethod(image,method);
                double newColor =  1.0 * getValueForPixel(image,row,column,method) / maxMethod * 255;
                Color tmp = new Color((int)newColor,(int)newColor,(int)newColor);
                newImage.setRGB(row, column,tmp.getRGB());
            }
        }

        return newImage;
       }

    private static double getValueForPixel(BufferedImage image, int row, int column, String method)
    {
        double colorValue = -1;
        double size = 3;

        while (colorValue == -1)
        {
            colorValue = getNearlestDistInBuff(image,row,column,method,size);
            size += 2;
        }

        return colorValue;
    }

    private static double getNearlestDistInBuff(BufferedImage image, int row, int column, String method, double size)
    {
        double distance =  maxValueMethod(image,method);
        int step = (int)size / 2;
        int beginX = getStart(row,step);
        int beginY = getStart(column,step);
        int endX = getEnd(image.getWidth(),row,step);
        int endY = getEnd(image.getHeight(),column, step);

        return getNearlestDistanceInFrame(image,row,column,beginX,endX,beginY,endY,method,distance);
    }

    private static int getEnd(int size, int x, int step)
    {
        int end = x + step;
        if ( end >= size)
        {
            int diff = end - size;
            return end - diff - 1;
        }
        return end;
    }

    private static int getStart(int x, int step)
    {
        int begin = x - step;
        if (begin < 0)
            return 0;
        return begin;
    }

    private static double getNearlestDistanceInFrame(BufferedImage image, int row, int column, int beginX, int endX, int beginY, int endY, String method, double distance)
    {
        distance = getNeaelestInRow(image,row, column,beginX, endX, beginY, method,distance);
        distance = getNeaelestInRow(image,row, column,beginX, endX, endY, method,distance);
        distance = getNeaelestInColumn(image,row,column,beginY,endY,beginX,method,distance);
        distance = getNeaelestInColumn(image,row,column,beginY,endY,endX,method,distance);

        return distance;
    }

    private static double maxValueMethod(BufferedImage image, String method)
    {
        if (method.equals("chessboard"))
            return chessboardMethodMax(image);
        else if ( method.equals("cityblock"))
            return cityblockMethodMax(image);
        else if ( method.equals("euclidean"))
            return euclideanMethodMax(image);
        else if ( method.equals("quasi-euclidean"))
            return quasiEuclideanMethodMax(image);
        else
            return image.getWidth() * image.getHeight();
    }

    private static double quasiEuclideanMethodMax(BufferedImage image)
    {

        if ( image.getHeight() > image.getWidth())
            return image.getWidth() + (Math.sqrt(2) - 1) * image.getHeight();
        else
            return (Math.sqrt(2) - 1) * image.getWidth() + image.getHeight();
    }

    private static double euclideanMethodMax(BufferedImage image)
    {
        return Math.sqrt( (image.getWidth()) * (image.getWidth()) + (image.getHeight()) * (image.getHeight()) );
    }

    private static double cityblockMethodMax(BufferedImage image)
    {
        return Math.abs(image.getWidth()) + Math.abs(image.getHeight());
    }

    private static double chessboardMethodMax(BufferedImage image)
    {
        return Math.max(Math.abs(image.getHeight()),Math.abs(image.getWidth()));
    }

    private static double getNeaelestInRow(BufferedImage image, int row, int column, int begin, int end, int constant, String method, double distance)
    {
        for (int i = begin; i <= end; i++)
        {
            Color color = new Color(image.getRGB(i, constant));
            if( color.getRed() != 0 )
            {
                double distanceTmp = doMethodBwdist(row,column,i,constant,method);
                if ( distanceTmp < distance )
                    distance = distanceTmp;
                if ( distance > 1400.0)
                    System.out.println(i + " " + constant);
            }
        }

        return distance;
    }

    private static double getNeaelestInColumn(BufferedImage image, int row, int column, int begin, int end, int constant, String method, double distance)
    {
        for (int i = begin; i <= end; i++)
        {
            Color color = new Color(image.getRGB(constant, i));
            if( color.getRed() != 0 )
            {
                double distanceTmp = doMethodBwdist(row,column,constant,i,method);
                if ( distanceTmp < distance )
                    distance = distanceTmp;
                if ( distance > 1400.0)
                    System.out.println(i + " " + constant);
            }
        }

        return distance;
    }

    private static double doMethodBwdist(int x1, int y1, int x2, int y2, String method)
    {
        if (method.equals("chessboard"))
            return chessboardMethod(x1,y1, x2, y2);
        else if ( method.equals("cityblock"))
            return cityblockMethod(x1,y1, x2, y2);
        else if ( method.equals("euclidean"))
            return euclideanMethod(x1,y1, x2, y2);
        else if ( method.equals("quasi-euclidean"))
            return quasiEuclideanMethod(x1,y1, x2, y2);
        else
            return 0.0;
    }

    private static double quasiEuclideanMethod(int x1, int y1, int x2, int y2)
    {
        double absDiffX = x1 - x2;
        double absDiffY = y1 - y2;

        if ( absDiffX > absDiffY)
            return absDiffX + (Math.sqrt(2) - 1) * absDiffY;
        else
            return (Math.sqrt(2) - 1) * absDiffX + absDiffY;
    }

    private static double euclideanMethod(int x1, int y1, int x2, int y2)
    {
        return Math.sqrt( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) );
    }

    private static double cityblockMethod(int x1, int y1, int x2, int y2)
    {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }

    private static double chessboardMethod(int x1, int y1, int x2, int y2)
    {
        return Math.max(Math.abs(x1 - x2),Math.abs(y1 - y2));
    }


}
