import java.awt.*;
import java.awt.image.BufferedImage;


public class Scale {
    public static BufferedImage scale(BufferedImage image,double coefficient)
    {
        if ( coefficient < 0)
            coefficient = 1 / (-coefficient);

        double heigh = image.getHeight() * coefficient;
        double width = image.getWidth() * coefficient;

        BufferedImage newImage = new BufferedImage((int)width, (int)heigh,BufferedImage.TYPE_INT_RGB);

        for (int row = 0; row < newImage.getWidth(); row++)
        {
            for (int column = 0; column < newImage.getHeight(); column++)
            {
                Color[][] colors = Kolory.getBufer(image, (int) (row / coefficient), (int)(column / coefficient), 4,4);
                double[][] newRed = Kolory.getRed(colors,4,4);
                double[][] newGreen = Kolory.getGreen(colors,4,4);
                double[][] newBlue = Kolory.getBlue(colors,4,4);

                double red = BiCubic.getBicubicValue(newRed,(row)/ newImage.getWidth(), (column )  / newImage.getHeight());
                double green = BiCubic.getBicubicValue(newGreen,(row ) / newImage.getWidth(), (column)  / newImage
                        .getHeight());
                double blue = BiCubic.getBicubicValue(newBlue,(row) / newImage.getWidth(), (column) / newImage
                        .getHeight());

                Color color = new Color((float)red, (float)green, (float)blue);

                newImage.setRGB(row, column,color.getRGB());
            }
        }

   /*     for (int row = 0; row < image.getWidth() ; row += 1)
        {
            for(int column = 0; column < image.getHeight(); column += 1)
            {
                Color[][] colors = Kolory.getBufer(image, row,column, 4, 4);
                double[][] newRed = Kolory.getRed(colors,4,4);
                double[][] newGreen = Kolory.getGreen(colors,4,4);
                double[][] newBlue = Kolory.getBlue(colors,4,4);


                for (int i =0; i < 4; i++)
                {
                    for (int j = 0; j< 4; j++)
                    {
                        double red = BiCubic.getBicubicValue(newRed,(row + i)/ image.getWidth(), (column + j)  / image.getHeight());
                        double green = BiCubic.getBicubicValue(newGreen,(row + i) / image.getWidth(), (column + j)  / image
                                .getHeight());
                        double blue = BiCubic.getBicubicValue(newBlue,(row + i) / image.getWidth(), (column + j) / image
                                .getHeight());

                        Color color = new Color((float)red, (float)green, (float)blue);

                        newImage.setRGB((int)((row + i) * coefficient), (int) ( (column +j) * coefficient ),color
                                .getRGB());
                    }
                }
            }
        }*/

        return newImage;
    }
}
