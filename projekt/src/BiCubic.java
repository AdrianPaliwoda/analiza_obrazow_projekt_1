public class BiCubic{

    public static double getCubicValue (double[] p, double x) {
        return p[1] + 0.5 * x*(p[2] - p[0] + x*(2.0*p[0] - 5.0*p[1] + 4.0*p[2] - p[3] + x*(3.0*(p[1] - p[2]) + p[3] - p[0])));
    }

    public static double getBicubicValue (double[][] p, double x, double y) {

        double buffer[] = new double[4];

        buffer[0] = getCubicValue(p[0], y);
        buffer[1] = getCubicValue(p[1], y);
        buffer[2] = getCubicValue(p[2], y);
        buffer[3] = getCubicValue(p[3], y);

        return getCubicValue(buffer, x);
    }
}
