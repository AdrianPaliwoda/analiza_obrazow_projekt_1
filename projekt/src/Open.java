import java.awt.image.BufferedImage;

/**
 * Created by adrian on 16/06/2017.
 */
public class Open {
    public static BufferedImage getOpne(BufferedImage image, double[][] structure)
    {
        return Dilation.getDilationBinary(Erosion.getErosionBinary(image,structure),structure);
    }
}
