Cele projektu:

    1. Zmina rozmiaru obrazka o zadanym współczynniku z wykorzystaniem interpolacji bikubicznej
    2. Filtracja entropii w zadanym oknie (tylko monochrom)
    3. Otwarcie kołowym elementem liniowym o zadanej długości i nachyleniu
    4. Mapa długości dla zadanej metryki(bwdist)

1.Zmina rozmiaru obrazka o zadanym współczynniku z wykorzystaniem interpolacji bikubicznej.

Obraz wejściowy:

![Scheme](projekt/p0.bmp)

Współczynnik 0.5:

![Scheme](projekt/p1.bmp)

2.Filtracja entropii w zadanym oknie (tylko monochrom)

Obraz wejściowy:

![Scheme](projekt/monochrome.jpg)

Obraz wynikowy – Entropia(9x9):

![Scheme](projekt/monoEntropy.jpg)

3. Otwarcie kołowym elementem liniowym o zadanej długości i nachyleniu

Obraz wejściowy:

![Scheme](projekt/binar1.png)

Wynik otwarcia (długość 8, kąt 45 stopni):

![Scheme](projekt/open.png)

4.Mapa długości dla zadanej metryki(bwdist)

Obraz wejściowy:

![Scheme](projekt/binar1.png)

Wynik dla chessboard:

![Scheme](projekt/bwdist.png)

Obraz wejściowy:

![Scheme](projekt/skaly.png)


Wynik dla euclidean:

![Scheme](projekt/bwdist2.png)
